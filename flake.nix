{
  description = "NixOS configuration";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    sops-nix = {
      url = "github:Mic92/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    firefox-addons = {
      url = "gitlab:rycee/nur-expressions?dir=pkgs/firefox-addons";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = {
    nixpkgs,
    home-manager,
    ...
  } @ inputs: {
    nixosConfigurations = {
      buddha = nixpkgs.lib.nixosSystem {
        modules = [
          ./hosts/buddha.nix
          ./modules
          {nixpkgs.hostPlatform = "x86_64-linux";}
          {nixpkgs.config.allowUnfree = true;}
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.senoraraton = import ./home-manager/default.nix;
            home-manager.extraSpecialArgs = {inherit inputs;};
            home-manager.sharedModules = [
              inputs.sops-nix.homeManagerModules.sops
            ];
          }
        ];
        specialArgs = {inherit inputs;};
      };
      samsara = nixpkgs.lib.nixosSystem {
        modules = [
          ./hosts/samsara.nix
          ./modules
          {nixpkgs.config.allowUnfree = true;}
          {nixpkgs.hostPlatform = "x86_64-linux";}
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.senoraraton = import ./home-manager/default.nix;
            home-manager.extraSpecialArgs = {inherit inputs;};
          }
        ];
      };
      moksha = nixpkgs.lib.nixosSystem {
        modules = [
          ./hosts/moksha.nix
          ./modules
          {nixpkgs.config.allowUnfree = true;}
          {nixpkgs.hostPlatform = "x86_64-linux";}
          home-manager.nixosModules.home-manager
          {
            home-manager.useGlobalPkgs = true;
            home-manager.useUserPackages = true;
            home-manager.users.senoraraton = import ./home-manager/default.nix;
            home-manager.extraSpecialArgs = {inherit inputs;};
          }
        ];
      };
    };
  };
}
