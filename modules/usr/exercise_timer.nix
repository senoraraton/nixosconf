{pkgs, ...}: let
  start_sound = builtins.path {
    path = ./standup.mp3;
    name = "standup.mp3";
  };
  end_sound = builtins.path {
    path = ./sitdown.mp3;
    name = "sitdown.mp3";
  };
in {
  systemd.user.services.exercise-start = {
    description = "Hourly exercise timer!";
    wantedBy = ["default.target"];
    serviceConfig = {
      Type = "simple";
      ExecStart = "${pkgs.writeShellScript "start_exercise_timer" ''
        #!/usr/bin/env sh

        paplay '${start_sound}' &
        systemctl --user start exercise-stop.timer
      ''}";
    };
  };

  systemd.user.services.exercise-stop = {
    description = "Excersie end sound/timer";
    wantedBy = ["default.target"];
    serviceConfig = {
      Type = "oneshot";
      ExecStart = "${pkgs.writeShellScript "complete_exercise_timer" ''
        #!/usr/bin/env sh
        paplay '${end_sound}' &
      ''}";
    };
  };

  systemd.user.timers.exercise-start = {
    description = "Timmer to trigger hourly exercise.";
    wantedBy = ["timers.target"];
    enable = true;
    timerConfig = {
      OnCalendar = "hourly";
      Unit = "exercise-start.service";
    };
  };

  systemd.user.timers.exercise-stop = {
    description = "Timer to play end sound after 10 minutes";
    wantedBy = ["timers.target"];
    timerConfig = {
      OnActiveSec = "10m";
      AccuracySec = "1s";
      Unit = "exercise-stop.service";
    };
    before = ["exercise-start.service"];
    requires = ["exercise-start.service"];
  };
}
