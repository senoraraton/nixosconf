{
  config,
  lib,
  pkgs,
  ...
}:
with lib; {
  options.three-d-print.enable = mkEnableOption "Enable 3d printing suite";

  config = mkIf config.three-d-print.enable {
    environment.systemPackages = with pkgs; [
      prusa-slicer
    ];
  };
}
