{
  lib,
  config,
  ...
}: {
  imports = [
  ];
  options.kde.enable = lib.mkEnableOption "Enable KDE w/ Xorg";

  config = lib.mkIf config.kde.enable {
    services.displayManager.sddm.enable = true;
    services.displayManager.sddm.wayland.enable = true;
    services.desktopManager.plasma6.enable = true;
    hardware.graphics.enable = true;
  };
}
