{
  lib,
  config,
  pkgs,
  ...
}: {
  options.hyprland.enable = lib.mkEnableOption "Enable hyprland";

  config = lib.mkIf config.hyprland.enable {
    programs.hyprland = {
      enable = true;
      xwayland.enable = true;
      #nvidiaPatches = true;
    };

    xdg.portal = {
      enable = true;
      xdgOpenUsePortal = true;
      extraPortals = [pkgs.xdg-desktop-portal-gtk];
    };

    users.users.senoraraton = {
      packages = with pkgs; [
        rofi-wayland
        grim
        slurp
        swappy
        clipman
        waybar
        xdg-desktop-portal-hyprland
        wlrctl
        mako
        eww
        swww
        polkit
        polkit_gnome
        gtk3
        qt5.full
        hyprland-protocols
        libsecret
        hyprland-workspaces
      ];
    };

    security.polkit.enable = true;
    services.gnome.gnome-keyring.enable = true;

    systemd = {
      user.services.polkit-gnome-authentication-agent-1 = {
        description = "polkit-gnome-authentication-agent-1";
        wantedBy = ["graphical-session.target"];
        wants = ["graphical-session.target"];
        after = ["graphical-session.target"];
        serviceConfig = {
          Type = "simple";
          ExecStart = "${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1";
          Restart = "on-failure";
          RestartSec = 1;
          TimeoutStopSec = 10;
        };
      };
    };

    security.polkit.extraConfig = ''

      polkit.addRule(function(action, subject) {
          if (
              subject.user == "senoraraton"
              && (action.id.indexOf("org.freedesktop.NetworkManager.") == 0
                  || action.id.indexOf("org.freedesktop.ModemManager") == 0)
          ) {
              return polkit.Result.YES;
          }
      });
        polkit.addRule(function(action, subject) {
          if (
            subject.isInGroup("users")
              && (
                action.id == "org.freedesktop.login1.reboot" ||
                action.id == "org.freedesktop.login1.reboot-multiple-sessions" ||
                action.id == "org.freedesktop.login1.power-off" ||
                action.id == "org.freedesktop.login1.power-off-multiple-sessions"
              )
            )
          {
            return polkit.Result.YES;
          }
        })
    '';

    environment.sessionVariables = {
      WLR_NO_HARDWARE_CURSORS = "1";
      NIXOS_OZONE_WL = "1";

      XDG_CURRENT_DESKTOP = "Hyprland";
      XDG_SESSION_TYPE = "wayland";
      XDG_SESSION_DESKTOP = "Hyprland";

      QT_AUTO_SCREEN_SCALE_FACTOR = 1;
      QT_QPA_PLATFORM = "wayland;xcb";
      QT_WAYLAND_DISABLE_WINDOWDECORATION = 1;
      QT_QPA_PLATFORMTHEME = "qt5ct";
      #QT_STYLE_OVERRIDE = "kvantum";
    };

    hardware = {
      graphics.enable = true;
      nvidia.modesetting.enable = true;
    };

    systemd.user.targets.hyprland-session = {
      unitConfig = {
        Description = "hyprland compositor session";
        BindsTo = ["graphical-session.target"];
        Wants = ["graphical-session-pre.target" "xdg-desktop-autostart.target"];
        After = ["graphical-session-pre.target"];
        Before = ["xdg-desktop-autostart.target"];
      };
    };

    systemd.user.services."irssi-close" = {
      description = "Deploy Irssi in Tmux peristence for logging.";
      requires = ["hyprland-session.target"];
      after = ["hyprland-session.target"];
      serviceConfig = {
        Type = "oneshot";
        RemainAfterExit = true;
        ExecStop = "${pkgs.tmux}/bin/tmux -S /tmp/tmux-1000/default kill-session -t irssi-pane";
      };
      wantedBy = ["default.target"];
    };
  };
}
