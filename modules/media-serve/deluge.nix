{
  config,
  lib,
  ...
}: {
  options.deluge.enable = lib.mkEnableOption "Enable deluge service";

  config = lib.mkIf config.deluge.enable {
    services.deluge = {
      enable = true;
      group = "media";
      dataDir = "/media/deluge";
      web = {
        enable = true;
        openFirewall = true;
      };
      declarative = true;
      authFile = "/media/deluge/.config/deluge/auth-write";
      openFirewall = true;
      config = {
        enabled_plugins = ["Label"];
      };
    };

    systemd.services.deluged = {
      requires = ["deluge-init-service.service"];
      after = ["deluge-init-service.service"];
    };

    systemd.services."deluge-init-service" = {
      script = ''
        username=$(cat "${config.sops.secrets."deluge/username".path}")
        password=$(cat "${config.sops.secrets."deluge/password".path}")
        mkdir -p /media/deluge
        echo "$username:$password:10" > /media/deluge/.config/deluge/auth
      '';
      serviceConfig = {
        User = "deluge";
        Type = "oneshot";
      };
    };
    sops.secrets."deluge/username" = {
      owner = "deluge";
    };
    sops.secrets."deluge/password" = {
      owner = "deluge";
    };
  };
}
