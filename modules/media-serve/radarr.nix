{
  config,
  lib,
  ...
}: {
  options.radarr.enable = lib.mkEnableOption "Enable radarr service";

  config = lib.mkIf config.radarr.enable {
    services.radarr = {
      enable = true;
      group = "media";
      dataDir = "/media/radarr";
      openFirewall = true;
    };
  };
}
