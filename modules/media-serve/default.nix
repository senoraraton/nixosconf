{
  config,
  lib,
  ...
}: {
  imports = [
    ./deluge.nix
    ./radarr.nix
    ./sonarr.nix
    ./jackett.nix
    ./jellyfin.nix
    #./jellyseer.nix
    ./bazarr.nix
  ];

  options.media-server.enable = lib.mkEnableOption "Enable full media server stack.";

  config = lib.mkIf config.media-server.enable {
    deluge.enable = true;
    radarr.enable = true;
    sonarr.enable = true;
    jackett.enable = true;
    jellyfin.enable = true;
    #jellyseer.enable = true;
    bazarr.enable = true;
  };
}
