{
  config,
  lib,
  ...
}: {
  options.jackett.enable = lib.mkEnableOption "Enable jackett service";

  config = lib.mkIf config.jackett.enable {
    services.jackett = {
      enable = true;
      group = "media";
      dataDir = "/media/jackett";
      openFirewall = true;
    };
  };
}
