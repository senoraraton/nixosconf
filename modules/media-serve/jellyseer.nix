{
  config,
  lib,
  ...
}: {
  options.jellyseer.enable = lib.mkEnableOption "Enable jellyseer service";

  config = lib.mkIf config.jellyseer.enable {
    services.jellyseer = {
      enable = true;
      openFirewall = true;
    };
  };
}
