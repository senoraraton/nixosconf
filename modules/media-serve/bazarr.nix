{
  config,
  lib,
  ...
}: {
  options.bazarr.enable = lib.mkEnableOption "Enable bazarr service";

  config = lib.mkIf config.bazarr.enable {
    services.bazarr = {
      enable = true;
      group = "media";
      openFirewall = true;
    };
  };
}
