{
  config,
  lib,
  ...
}: {
  options.jellyfin.enable = lib.mkEnableOption "Enable jellyfin service";

  config = lib.mkIf config.jellyfin.enable {
    services.jellyfin = {
      enable = true;
      group = "media";
      dataDir = "/media/jellyfin/data";
      logDir = "/media/jellyfin/logs";
      cacheDir = "/media/jellyfin/.cache";
      configDir = "/media/jellyfin/.config";
      openFirewall = true;
    };
  };
}
