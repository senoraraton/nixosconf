{
  config,
  lib,
  ...
}: {
  options.sonarr.enable = lib.mkEnableOption "Enable sonarr service";

  config = lib.mkIf config.sonarr.enable {
    services.sonarr = {
      enable = true;
      group = "media";
      dataDir = "/media/sonarr";
      openFirewall = true;
    };
  };
}
