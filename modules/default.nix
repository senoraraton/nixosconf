{...}: {
  imports = [
    ./display
    ./ops/k8.nix
    ./ops/QEMU.nix
    ./ops/docker.nix
    ./3dprint/3dprint.nix
    ./games.nix
    ./nvim
    ./media-serve
    ./discord.nix
    ./network-utils.nix
    ./usr/exercise_timer.nix
  ];
}
