{
  config,
  lib,
  pkgs,
  ...
}: {
  options.docker.enable = lib.mkEnableOption "Enable docker/podman";

  config = lib.mkIf config.docker.enable {
    environment.systemPackages = with pkgs; [
      podman-compose
      podman
    ];
    virtualisation = {
      podman = {
        enable = true;
        # Create a `docker` alias for podman, to use it as a drop-in replacement
        dockerCompat = true;

        # Required for containers under podman-compose to be able to talk to each other.
        defaultNetwork.settings.dns_enabled = true;
      };
    };
  };
}
