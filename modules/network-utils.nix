{
  config,
  lib,
  pkgs,
  ...
}: {
  options.network-debug.enable = lib.mkEnableOption "Enable network debugging suite";

  config = lib.mkIf config.network-debug.enable {
    users.users.senoraraton = {
      packages = builtins.attrValues {
        inherit
          (pkgs)
          wireshark
          traceroute
          nettools
          nmap
          ;
      };
    };
  };
}
