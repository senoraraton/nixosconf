{
  config,
  lib,
  pkgs,
  ...
}: {
  options.games.enable = lib.mkEnableOption "Enable games";

  config = lib.mkIf config.games.enable {
    programs.steam = {
      enable = true;
      package = with pkgs; steam.override {extraPkgs = pkgs: [attr];};
    };
    users.users.senoraraton = {
      packages = builtins.attrValues {
        inherit
          (pkgs)
          duckstation
          pcsx2
          mgba
          #tintin
          
          #sqlite
          
          gzdoom
          #steam
          
          #cataclysm-dda-git
          
          ;
      };
    };
  };
}
