{
  pkgs,
  lib,
  ...
}: let
  myNeovim = let
    neovimConfig = pkgs.neovimUtils.makeNeovimConfig {
      plugins =
        [pkgs.vimPlugins.nvim-treesitter.withAllGrammars]
        ++ lib.mapAttrsToList (
          pname: v: (pkgs.vimUtils.buildVimPlugin {
            inherit pname;
            version = builtins.substring 0 8 v.revision;
            src = v.outPath;
            doCheck = false;
          })
        )
        (lib.filterAttrs (name: _:
          builtins.elem name [
            #System
            "nvim-lspconfig"
            "plenary.nvim"
            "undotree"
            "telescope.nvim"
            "nvim-treesitter-textojbects"
            "lualine.nvim"
            "popup"
            "nvim-web-devicons"
            "cmp-nvim-lsp"
            "cmp-buffer"
            "cmp-path"
            "cmp-cmdline"
            "cmp-nvim-lua"
            "nvim-cmp"
            "node-ls.nvim"
            "cmp_luasnip"
            "LuaSnip"
            "telescope-fzf-native.nvim"
            "none-ls.nvim"
            #LSP
            "emmet-vim"
            "go.nvim"
            "vim-closetag"
            "prettier.nvim"
            #Theme
            "vim-dogrun"
            "space-vim-theme"
            "leaf.nvim"
            "neovim" #Rose pine
            "tokyonight.nvim"
            #Misc
            "nerdcommenter"
            "vim-fugitive"
            "playground"
            "vim-startuptime"
            "vim-tmux-navigator"
          ]) (import ../../npins));
      withPython3 = true;
      extraPython3Packages = _: [];
      viAlias = false;
      vimAlias = false;
      customRC = import ./lua;
    };
    wrapperArgs = let
      path = lib.makeBinPath (
        with pkgs; [
          #Nix
          deadnix
          statix
          alejandra
          nixd
          nil

          #Lua
          lua-language-server
          stylua
          luajitPackages.luacheck

          #Typescript
          nodePackages_latest.typescript-language-server
          nodePackages_latest.prettier

          #Go
          gopls
          gofumpt
          gotools
          go-outline
          gopkgs
          godef
          golint
          delve
          ginkgo
          richgo
          gotestsum
          #C
          clang
          clang-tools
          cppcheck
          checkmake
          vscode-extensions.ms-vscode.cpptools
          #Ops
          ansible-language-server
          hadolint
          yamllint
          terraform-ls
          tflint
          vimPlugins.gentoo-syntax
          shellcheck
          nodePackages_latest.bash-language-server
          sqls
          postgres-lsp
          #Frontend
          vscode-langservers-extracted
          #Misc
          haskell-language-server
          zls
        ]
      );
    in
      neovimConfig.wrapperArgs
      ++ [
        "--prefix"
        "PATH"
        ":"
        path
        "--prefix"
        "LUA_PATH"
        ";"
        "${./lua}/?.lua"
      ];
  in
    pkgs.wrapNeovimUnstable pkgs.neovim-unwrapped (neovimConfig // {inherit wrapperArgs;});
in {
  environment.systemPackages = [myNeovim];
}
