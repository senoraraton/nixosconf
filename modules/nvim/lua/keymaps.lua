local keymaps = {}

local opts = { noremap = true, silent = true }
local term_opts = { silent = true }
local keymap = vim.api.nvim_set_keymap
--Leader
keymap("", "<Space>", "<Nop>", opts)
vim.g.mapleader = " "
vim.g.maplocalleader = " "

--Normal
keymap("n", "<C-d>", "<C-d>zz", opts)
keymap("n", "<C-u>", "<C-u>zz", opts)

keymap("n", "<A-h>", ":wincmd h<CR>", opts)
keymap("n", "<A-j>", ":wincmd j<CR>", opts)
keymap("n", "<A-k>", ":wincmd k<CR>", opts)
keymap("n", "<A-l>", ":wincmd l<CR>", opts)
keymap("n", "<leader>f", ":Lex 15<cr><cr>", opts)

keymap("n", "<C-Up>", ":resize -2<CR>", opts)
keymap("n", "<C-Down>", ":resize +2<CR>", opts)
keymap("n", "<C-Left>", ":vertical resize -2<CR>", opts)
keymap("n", "<C-Right>", ":vertical resize +2<CR>", opts)

--Visual

--Hold buffer on yank/paste
vim.keymap.set("x", "<leader>p", [["_dP]])

vim.keymap.set({ "n", "v" }, "<leader>y", [["+y]])
vim.keymap.set("n", "<leader>Y", [["+Y]])

vim.keymap.set({ "n", "v" }, "<leader>d", [["_d]])

--Stay in indent mode
keymap("v", "<", "<gv", opts)
keymap("v", ">", ">gv", opts)

--Term
keymap("t", "<Esc>", "<C-\\><C-n>", term_opts)

--Paren completion
keymap("i", "{", "{}<Esc>ha", opts)
keymap("i", "{{", "{<CR>}<Esc>ko", opts)
keymap("i", "(", "()<Esc>ha", opts)
keymap("i", "[", "[]<Esc>ha", opts)
keymap("i", '"', '""<Esc>ha', opts)
keymap("i", "'", "''<Esc>ha", opts)
keymap("i", "`", "``<Esc>ha", opts)
vim.cmd([[:inoremap <expr> ; search('\%#[]>)}''"]', 'n') ? '<Right>' : ';']])

-- LSP
function keymaps.lsp_maps(_, bufnr)
	vim.api.nvim_buf_set_option(0, "omnifunc", "v:lua.vim.lsp.omnifunc")
	local bufopts = { noremap = true, silent = true, buffer = bufnr }
	vim.keymap.set("n", "K", vim.lsp.buf.hover, bufopts)
	vim.keymap.set("n", "ds", ":vsplit | lua vim.lsp.buf.implementation()<CR>", bufopts)
	vim.keymap.set("n", "df", ":belowright split | lua vim.lsp.buf.definition()<CR>", bufopts)
	vim.keymap.set("n", "dt", vim.lsp.buf.type_definition, bufopts)
	vim.keymap.set("n", "di", vim.lsp.buf.implementation, bufopts)
	vim.keymap.set("n", "dn", vim.diagnostic.goto_next, bufopts)
	vim.keymap.set("n", "dp", vim.diagnostic.goto_prev, bufopts)
	vim.keymap.set("n", "<leader>d", ":Telescope diagnostics<cr>", bufopts)
	vim.keymap.set("n", "vr", vim.lsp.buf.rename, bufopts)
	vim.keymap.set("n", "pp", vim.lsp.buf.code_action, bufopts)
end

-- DaP
function keymaps.dap_maps()
	vim.keymap.set("n", "<F4>", ":lua require('dapui').open()<cr>")
	vim.keymap.set("n", "<F5>", ":lua require('dap').continue()<cr>")
	vim.keymap.set("n", "<F10>", ":lua require'dap'.step_over()<cr>")
	vim.keymap.set("n", "<F11>", ":lua require'dap'.step_into()<cr>")
	vim.keymap.set("n", "<F12>", ":lua require'dap'.step_out()<cr>")
	vim.keymap.set("n", "<leader>b", ":lua require'dap'.toggle_breakpoint()<cr>")
	vim.keymap.set("n", "<leader>B", ":lua require'dap'.set_breakpoint(vim.fn.input('Breakpoint condition: '))<cr>")
	vim.keymap.set(
		"n",
		"<leader>lp",
		":lua require'dap'.set_breakpoint(nil,nil, vim.fn.input('Log point message: '))<cr>"
	)
	vim.keymap.set("n", "<leader>dr", ":lua require'dap'.repl.open()<cr>")
end

vim.keymap.set("n", "<leader>u", vim.cmd.UndotreeToggle)
vim.keymap.set("n", "<leader>gs", vim.cmd.Git)

local builtin = require("telescope.builtin")
vim.keymap.set("n", "<leader>fd", builtin.find_files, {})
vim.keymap.set("n", "<leader>ff", builtin.git_files, {})
vim.keymap.set("n", "<leader>fg", builtin.live_grep, {})
vim.keymap.set("n", "<leader>fs", builtin.grep_string, {})
vim.keymap.set("n", "<leader>fb", builtin.buffers, {})

--Folding
local function toggle_folds()
	local foldlevel = vim.api.nvim_eval("&foldlevel")
	if foldlevel == 99 then
		vim.cmd("silent! %foldc!") -- Close all folds if they are open (foldlevel is 99)
		vim.opt.foldlevel = 0 -- Set foldlevel to 0 (closed state)
	else
		vim.cmd("silent! %foldo!") -- Open all folds if they are closed
		vim.opt.foldlevel = 99 -- Set foldlevel to 99 (open state)
	end
end

vim.keymap.set("n", "<leader>o", toggle_folds, { desc = "Toggle all folds." })

vim.keymap.set("n", "<c-h>", "<cmd><C-U>TmuxNavigateLeft<cr>")
vim.keymap.set("n", "<c-j>", "<cmd><C-U>TmuxNavigateDown<cr>")
vim.keymap.set("n", "<c-k>", "<cmd><C-U>TmuxNavigateUp<cr>")
vim.keymap.set("n", "<c-l>", "<cmd><C-U>TmuxNavigateRight<cr>")
vim.keymap.set("n", "<c-\\>", "<cmd><C-U>TmuxNavigatePrevious<cr>")

local def_win = nil
local def_buf = nil
local stored_bufnr = nil

function keymaps.toggle_floating_def()
	if def_win and vim.api.nvim_win_is_valid(def_win) then
		vim.api.nvim_win_close(def_win, true)
		def_win = nil
	else
		if stored_bufnr and vim.api.nvim_buf_is_valid(stored_bufnr) then
			def_win = vim.api.nvim_open_win(stored_bufnr, true, {
				relative = "cursor",
				width = math.floor(vim.o.columns * 0.5),
				height = math.floor(vim.o.lines * 0.4),
				col = 1,
				row = 1,
				style = "minimal",
				border = "roudned",
			})
		end
	end
end

function keymaps.open_definition_floating(open_window)
	local params = vim.lsp.util.make_position_params()
	vim.lsp.buf_request(0, "textDocument/implementation", params, function(_, result, ctx, _)
		if not result or vim.tbl_isempty(result) then
			return
		end
		vim.api.nvim_out_write("Hello, Neovim!\n")

		local def = result[1] or result
		local uri = def.uri or def.targetUri
		local range = def.range or def.targetSelectionRange
		local bufnr = vim.uri_to_bufnr(uri)

		-- Load buffer if it's not already loaded
		vim.fn.bufload(bufnr)

		-- Create or reuse a buffer to store the definition
		if not def_buf or not vim.api.nvim_buf_is_valid(def_buf) then
			def_buf = vim.api.nvim_create_buf(false, true) -- Create a scratch buffer
		end

		-- Copy contents of the definition buffer into the floating buffer
		vim.api.nvim_buf_set_lines(def_buf, 0, -1, false, vim.api.nvim_buf_get_lines(bufnr, 0, -1, false))

		-- Store the latest definition buffer
		stored_bufnr = def_buf

		-- If open_window is true, also open the floating window
		if open_window then
			toggle_floating_def()
		end
	end)
end

return keymaps
