vim.g.problem_languages = { "c", "golang", "typescript", "lua" }

-- In lua/project_root.lua

-- Function to get the Git root directory
local function get_git_root()
	local handle = io.popen("git rev-parse --show-toplevel 2>/dev/null")
	local git_root = handle:read("*a"):gsub("%s+", "")
	handle:close()
	return git_root
end

-- Function to set the working directory if it differs from Git root
local function check_and_set_cwd()
	local git_root = get_git_root()
	if git_root and git_root ~= "" then
		local current_dir = vim.fn.getcwd()
		if current_dir ~= git_root then
			vim.cmd("cd " .. git_root)
		end
	end
end

vim.api.nvim_create_autocmd("BufReadPost", {
	pattern = "*.lua",
	callback = function()
		check_and_set_cwd()
	end,
})
