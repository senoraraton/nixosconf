{pkgs, ...}: {
  home.packages = with pkgs; [
    ffmpeg
    mpv
    btop
    figlet
    ranger
    npins
    pavucontrol
    bc
    wl-clipboard
    #arduino-cli
    #any-nix-shell
  ];
}
