{
  pkgs,
  config,
  ...
}: {
  programs.irssi = {
    enable = true;
    extraConfig = ''
          settings = {
            core = {
              real_name = "Claus Mouse";
              user_name = "senoraraton";
              nick = "senoraraton";
            };
          "fe-common/core" = { autolog_colors = "no"; awaylog_colors = "no"; };
      };
    '';
  };
  sops.secrets."irssi/username" = {};
  sops.secrets."irssi/password" = {};

  home.file = {
    ".config/kitty/irssi-init.conf" = {
      text = ''
        cd ~/.irrsi
        launch --title "irssi-pane" sh -c 'tmux new-session -A -s irssi-pane\; send-keys "irssi" C-m'
      '';
    };
  };

  home.file = {
    ".irssi/scripts/autorun/init.pl".source =
      pkgs.writers.writePerl "irssi-init" {}
      ''

        use strict;
        use vars qw($VERSION %IRSSI);

        use Irssi;
        $VERSION = '1.4.5';

        %IRSSI = (
            authors     => 'Claus Mouse',
            contact     => 'senoraraton@gmail.com',
            name        => 'Irssi SASL init',
            description => 'Inits Sasl',
            license     => 'GPL 2.0',
        );

        sub read_file {
            my ($filepath) = @_;
            open my $fh, '<', $filepath or die "Could not open '$filepath' $!";
            chomp(my $content = <$fh>);
            close $fh;
            return $content;
        }

        sub sig_channel_join {
            my ($channel) = @_;
            my $name = $channel->{name};
            $name =~ s/^#//;
            my $path = "$ENV{HOME}/.irssi/logs/$name";
            mkdir $path unless -d $path;
            Irssi::command("LOG OPEN -targets $name -auto $path/%Y-%m-%d.log");
        }

        sub sig_private_message {
            my ($server, $msg, $nick, $address) = @_;
            my $path = "$ENV{HOME}/.irssi/logs/privmsgs";
            mkdir $path unless -d $path;
            Irssi::command("LOG OPEN -targets $nick -auto $path/$nick.log");
        }

        my $username = read_file('${config.sops.secrets."irssi/username".path}');
        my $password = read_file('${config.sops.secrets."irssi/password".path}');

        my @commands = (
            "NETWORK add -sasl_username $username -sasl_password $password -sasl_mechanism PLAIN LiberaChat",
            'SERVER add -auto -net LiberaChat -tls -tls_verify irc.libera.chat 6697',
            'IGNORE * JOINS PARTS QUITS NICKS',
            'CONNECT irc.libera.chat 6697',
        );

        Irssi::signal_add('setup changed', sub {
            for my $cmd (@commands) {
                Irssi::command($cmd);
            }
        });
        Irssi::signal_emit('setup changed');

        Irssi::signal_add('channel joined', 'sig_channel_join');
        Irssi::signal_add('message private', 'sig_private_message');
      '';
  };
}
