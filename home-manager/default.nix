{...}: {
  home.stateVersion = "22.11";
  imports = [
    ./git.nix
    ./zsh.nix
    ./kitty.nix
    ./firefox.nix
    ./usr-pkgs.nix
    ./zathura.nix
    ./hyprland.nix
    ./kanshi.nix
    ./irssi.nix
    ./waybar.nix
    ./gtk.nix
    ./tmux.nix
  ];
  sops = {
    age.keyFile = "/home/senoraraton/.config/sops/age/keys.txt";
    defaultSopsFile = ../secrets/secrets.yaml;
  };
}
