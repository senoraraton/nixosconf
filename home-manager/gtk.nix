{pkgs, ...}: {
  fonts.fontconfig.enable = true;
  home.packages = with pkgs; [
    nerd-fonts.jetbrains-mono
    nerd-fonts.fira-code
    cascadia-code
    nerd-fonts.symbols-only
    twemoji-color-font
    noto-fonts-emoji
  ];

  gtk = {
    enable = true;
    font = {
      name = "CaskaydiaCove Nerd Font";
      size = 12;
    };
    theme = {
      name = "Tokyo-night-gtk";
      package = pkgs.kanagawa-gtk-theme;
    };
    #iconTheme = {
    #name = "Papirus-Dark";
    #package = pkgs.papirus-icon-theme.override {
    #color = "black";
    #};
    #};
    cursorTheme = {
      name = "Bibata-Modern-Ice";
      package = pkgs.bibata-cursors;
      size = 24;
    };
  };

  home.pointerCursor = {
    name = "Bibata-Modern-Ice";
    package = pkgs.bibata-cursors;
    size = 24;
  };
}
