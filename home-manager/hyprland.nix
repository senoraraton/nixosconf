{
  pkgs,
  lib,
  ...
}: let
  activeBorder = "0xe5b323ad";
  inactiveBorder = "0xe01773e0";

  startupScript =
    pkgs.writeShellScriptBin "hypr-init"
    ''
      ${pkgs.waybar}/bin/waybar &
      ${pkgs.clipman}/bin/clipman &
      systemctl --user start xdg-desktop-portal-hyprland
      #${pkgs.polkit_gnome}/libexec/polkit-gnome-authentication-agent-1 &
      nohup kitty --title "irssi-pane" --session irssi-init.conf & disown
      hyprctl setcursor Bibata-Modern-Ice 24 &
    '';
in {
  wayland.windowManager.hyprland = {
    enable = true;
    extraConfig = ''
            exec = systemctl --user start hyprland-session.target
            exec = systemctl --user restart kanshi.service
            exec-once = ${pkgs.discord}/bin/discord
            exec-once = ${startupScript}/bin/hypr-init

            $mod = SUPER;
            general {
                gaps_in=2
                gaps_out=4
                border_size=3
                col.active_border=${activeBorder}
                col.inactive_border=${inactiveBorder}
            }
            dwindle {
                force_split=2
            }

            input {
              repeat_rate=50
              repeat_delay=300
              follow_mouse=2

                touchpad {
                  natural_scroll=true
                }
            }

           decoration {
            rounding= 8
            dim_inactive=true
            dim_strength=0.2
            blur {
              enabled=false
            }
          }

      misc {
        disable_hyprland_logo=true
        disable_splash_rendering=true
      }

      animation=global,1,2,default
      animation=windows,1,2,default,slide

      # ====== Window Rules ====== #
      windowrulev2 = workspace 9 silent, class:^(kitty)$, title:^(irssi-pane)$
      windowrulev2 = workspace 8 silent, class:^(discord)$, title:^(Discord)$

      # ======= Workspaces ======= #
      workspace=1,monitor:DP-5
      workspace=2,monitor:DP-4
      workspace=3,monitor:DP-5

      ${lib.concatMapStringsSep "\n"
        (n: "workspace=${toString n},monitor:DP-4") (lib.range 4 9)}


      # ======== BINDINGS ======== #
      # ======== Mouse ======== #
      bindm=SUPER,mouse:272,movewindow
      bindm=SUPER,mouse:273,resizewindow

      # ======== Shortcuts ======== #

      bind=SUPER,p,exec,rofi -modi drun -show drun -show-icons -matching fuzzy -drun-match-fields name -display-drun 'launch: '
      bind=SUPER,Return,exec,$TERM
      bind=SUPER,M,exec,wpctl status | grep -q "MUTED" && wpctl set-mute 51 0 || wpctl set-mute 51 1


      # Screenshot
      bind=SUPER,g,exec,slurp | grim -g -$(xdg-user-dir PICTURES)/$(date + 'screenshot_%Y-%m-%d-%H%M%S.png')
      bind=SUPERSHIFT,g,exec,${pkgs.grim} -g "$(${pkgs.slurp} -b 28282877 -c d64d0e)" - | ${pkgs.swappy} -f -

      # Screen zoom thing
      bind=SUPER,x,exec,grim - | imv -f -c 'bind <Escape> quit' -

      # ======== Window management ======== #
      bind=SUPER,f,fullscreen,0
      bind=SUPERSHIFT,f,togglefloating,
      bind=SUPER,t,togglegroup,
      bind=SUPER,c,changegroupactive

      bind=SUPER,Escape,exec,systemctl suspend
      bind=SUPERSHIFT,Escape,exit,
      bind=SUPER,q,killactive,

      # ======== Window navigation ======== #
      bind=SUPER,left,movefocus,l
      bind=SUPER,right,movefocus,r
      bind=SUPER,up,movefocus,u
      bind=SUPER,down,movefocus,d

      bind=SUPER,h,movefocus,l
      bind=SUPER,l,movefocus,r
      bind=SUPER,k,movefocus,u
      bind=SUPER,j,movefocus,d

      binde=SUPERSHIFT,left,resizeactive,-32 0
      binde=SUPERSHIFT,right,resizeactive,32 0
      binde=SUPERSHIFT,up,resizeactive,0 -32
      binde=SUPERSHIFT,down,resizeactive,0 32

      binde=SUPERCONTROL,h,resizeactive,-32 0
      binde=SUPERCONTROL,l,resizeactive,32 0
      binde=SUPERCONTROL,k,resizeactive,0 -32
      binde=SUPERCONTROL,j,resizeactive,0 32

      bind=SUPERSHIFT,left,movewindow,l
      bind=SUPERSHIFT,right,movewindow,r
      bind=SUPERSHIFT,up,movewindow,u
      bind=SUPERSHIFT,down,movewindow,d

      ${lib.concatMapStringsSep "\n"
        (n: "bind=SUPER,${toString n},workspace,${toString n}") (lib.range 1 9)}
      ${lib.concatMapStringsSep "\n"
        (n: "bind=SUPER:SHIFT,${toString n},movetoworkspace,${toString n}")
        (lib.range 1 9)}
      ${lib.concatMapStringsSep "\n"
        (n: "bind=SUPERCONTROL,${toString n},movetoworkspacesilent,${toString n}") (lib.range 1 9)}
    '';
  };
}
