{pkgs, ...}: {
  home.file = {
    "/home/senoraraton/.config/tmux/term_popup.sh" = {
      text = ''
        #!/usr/bin/env bash

        # $1 == session_name
        # $2 == current directory of pane

        if [[ "$1" == "popup" ]]; then
            tmux detach-client -s "popup"
        else
            tmux new-session -d -s "popup" -c "$2" 2>/dev/null || true
            tmux set-option -t "popup" detach-on-destroy on 2>/dev/null || true
            tmux setw -t "popup" status off
            tmux display-popup -w 80% -h 80% -E "tmux new-session -D -A -s 'popup' -c '$2'"
        fi
      '';
      executable = true;
    };
  };

  programs.tmux = {
    enable = true;
    clock24 = true;
    mouse = false;
    newSession = true;
    historyLimit = 5000;
    terminal = "xterm-kitty";
    keyMode = "vi";
    plugins = with pkgs.tmuxPlugins; [
      vim-tmux-navigator
      {
        plugin = dracula;
        extraConfig = ''
          set -g @dracula-show-powerline true
            set -g @dracula-refresh-rate 10
            set -g @dracula-show-left-icon session
            set -g @dracula-show-empty-plugins false
            set -g @dracula-show-location false
            set -g @dracula-show-battery false
            set -g @dracula-git-disable-status true
            set -g @dracula-show-timezone false
            set -g @dracula-military-time true
            set -g @dracula-time-format "%m/%d %H:%M"
            set -g @dracula-plugins "git ssh-session network-ping time"
        '';
      }
    ];
    extraConfig = ''
      set-option -g prefix C-space

      set -g base-index 1
      setw -g pane-base-index 1

      bind -n C-l send-keys 'C-l'

      bind -Troot C-w switch-client -T VimWindowMovements
      bind -TVimWindowMovements h select-pane -L
      bind -TVimWindowMovements j select-pane -D
      bind -TVimWindowMovements k select-pane -U
      bind -TVimWindowMovements l select-pane -R


      bind -N 'Popup' 't' {
          run-shell "~/.config/tmux/term_popup.sh '#{session_name}' '#{pane_current_path}'"
      }

    '';
  };
}
