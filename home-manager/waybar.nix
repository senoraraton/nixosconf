{...}: {
  home.file = {
    ".config/waybar/config.jsonc".text = ''
          {
          "layer": "top",
          "height": 25,
            "modules-left": ["hyprland/workspaces"],
          	"modules-center": ["clock"],
            "modules-right": ["pulseaudio", "custom/left-arrow-dark", "network", "disk" ],
            "hyprland/window": {
                    "max-length": 200,
                        "separate-outputs": true
            },
          	"hyprland/workspaces": {
          	"format": "{name} {icon}",
          	"tooltip": false,
          	"all-outputs": true,
          	"format-icons": {
          		"active": "",
          		"default": ""
          	},
          	"on-scroll-up": "hyprctl dispatch workspace e-1",
          	"on-scroll-down": "hyprctl dispatch workspace e+1",
          	"on-click": "activate"
          	},
          	"disk": {
          		"format": " {percentage_used}% ({free})",
          		"tooltip": true,
          		"interval": 2,
          		"on-click":"kitty sh -c 'ranger'"
          	},
          	"clock": {
          		"format": "  {:%d <small>%a</small> %H:%M}",
          		//"format": " {:%a %b %d %Y | %H:%M}",
          		"format-alt": "  {:%A %B %d %Y (%V) | %r}",
          		"tooltip-format": "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>",
          		"calendar-weeks-pos": "right",
          		"today-format": "<span color='#f38ba8'><b><u>{}</u></b></span>",
          		"format-calendar": "<span color='#f2cdcd'><b>{}</b></span>",
          		"format-calendar-weeks": "<span color='#94e2d5'><b>W{:%U}</b></span>",
          		"format-calendar-weekdays": "<span color='#f9e2af'><b>{}</b></span>",
          		"interval": 60
          	},
          	"pulseaudio": {
          		// "scroll-step": 1, // %, can be a float
          		"format": "{icon} {volume}%", // {format_source}
          		"format-bluetooth": "{icon} {volume}%", // {format_source}
          		"format-bluetooth-muted": "󰗿", // {format_source}
          		"format-muted": "", // {format_source}
          		"format-source": "{volume}% ",
          		"format-source-muted": "",
          		"format-icons": {
          			"headphone": "󰋋",
          			"headset": "󰋎",
          			"phone": "",
          			"portable": "",
          			"car": " ",
          			"default": [
          				"",
          				"",
          				" "
          			]
          		},
          		"on-click": "pavucontrol"
          	},
          	"bluetooth": {
          	"format": "<span color='#0056A3'></span> {status}",
          	"format-disabled": "", // an empty format will hide the module
          	"format-connected": "<span color='#0056A3'></span> {num_connections}",
          	"tooltip-format": "{device_enumerate}",
          	"tooltip-format-enumerate-connected": "{device_alias}   {device_address}"
          	},
          	"network": {
          		"interface": "wlp2s0",
          		"format": "󰱓 {bandwidthTotalBytes}",
          		"format-disconnected": "{icon} No Internet",
          		"format-linked": "󰅛 {ifname} (No IP)",
          		"format-alt": "󰛶 {bandwidthUpBytes} | 󰛴 {bandwidthDownBytes}",
          		"tooltip-format": "{ifname}: {ipaddr}/{cidr} Gateway: {gwaddr}",
          		"tooltip-format-wifi": "{icon} {essid} ({signalStrength}%)",
          		"tooltip-format-ethernet": "{icon} {ipaddr}/{cidr}",
          		"tooltip-format-disconnected": "{icon} Disconnected",
          		"format-icons": {
          			"ethernet": "󰈀",
          			"disconnected": "⚠",
          			"wifi": [
          				"󰖪",
          				""
          			]
          		},
          		"interval": 2
          	},
        "custom/left-arrow-dark": {
      "format": "",
      "tooltip": false
                },
          }
    '';

    ".config/waybar/style.css".text = ''
               * {
           	font-family: 'M+1Code Nerd Font';
           	font-size: 12px;
           	min-height: 14px;
           }

           window#waybar {
           	background: transparent;
           }

           #workspaces {
           	background-color: transparent;
           	color: #0d74bd;
           	margin-right: 15px;
           	padding-left: 10px;
           	padding-right: 10px;
           }

           #workspaces button.active {
           	background: transparent;
           	color: #0d74bd;
           }

           #disk {
           	color: #A8A8A8;

           }

           #network {
           	color: #10a140;
           	padding-left: 5px;
           }

           #pulseaudio {
           	color: #ba23d9;
           	padding-left: 5px;
           }

           #clock {
           	color: #00ba69;
           }
           #custom-right-arrow-dark,
           #custom-left-arrow-dark {
           	color: #292b2e;
      background: #1a1a1a;
           }
    '';
  };
}
