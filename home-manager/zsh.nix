{
  pkgs,
  inputs,
  ...
}: {
  programs.direnv = {
    enable = true;
    enableZshIntegration = true;
    nix-direnv.enable = true;
  };
  programs.zsh = {
    enable = true;
    autocd = true;
    dotDir = ".config/zsh";
    enableCompletion = true;
    history.path = "$HOME/.hf";
    history.save = 10000;
    history.size = 10000;
    history.share = true;
    history.extended = true;
    history.ignoreSpace = true;

    completionInit = ''
      zstyle :compinstall filename '/home/senoraraton/.config/zsh/.zshrc'
      zstyle ':vcs_info:git:*' formats '[%b]'
      autoload -Uz compinit
      autoload -U colors && colors
      autoload -Uz vcs_info
      precmd() {vcs_info}
      compinit
    '';

    shellAliases = {
      ls = "ls -h --color=auto --group-directories-first";
      ll = "ls -alF";
      l = "ls -A";
      la = "ls -CF --color=auto";
      less = "less -R";
      watch = "watch --color";
      sudoe = "sudo -E -s";
      tb = "nc termbin.com 9999";
      pingt = "ping -c 5 google.com";
      pingd = "ping -c 5 8.8.8.8";
      gitlogold = "git log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(auto)%d%C(reset)'";
      gitlog = "git log --graph --abbrev-commit --decorate --date=format:'(%m_%d)' --format=format:'%C(bold blue)%h%C(reset) %C(bold green)%ad%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(auto)%d%C(reset)'";
      gitlines = "git ls-files | xargs wc -l";
      dirsize = "du -sh $PWD/*";
      nixbuild = "sudo nixos-rebuild switch --flake \"/home/senoraraton/bins/nixosconf/#\"";
      n = "nvim";
      k = "kubectl";
      pc = "podman-compose";
      kpods = "kubectl get pods --all-namespaces | grep -v 'kube-system'";
      kbox = "kubectl run temp-pod --rm -i --tty --image=busybox -- /bin/sh";
      cat = "bat --style=plain --paging=never";
      wcdir = ''find . -type f | xargs -I {} sh -c 'lines=$(cat "{}" | sed "/^\s*#/d;/^\s*$/d" | wc -l); echo "$lines {}"' | tee >(awk '{sum += $1} END {print "total", sum}')'';

      startx = "Hyprland";
      sgp = "~/bins/supergenpass-bash/sgp-bash.sh";
      checkguards = ''find . -type f -name "*.h" -exec grep -L '^#ifndef' {} +'';
    };

    sessionVariables = {
      LANG = "en_US.UTF-8";
      TERM = "kitty";
      EDITOR = "nvim";
      PATH = "~/.npm-packages/bin:$PATH";
      NODE_PATH = "~/.npm-packages/lib/node_modules";
      RPROMPT = "%F{57}\${vcs_info_msg_0_}%f%b";
      DIRENV_LOG_FORMAT = "";
      READNULLCMD = "bat_read_null";
      BAT_THEME = "Solarized (dark)";
    };

    initExtra = ''
      setopt extendedglob nomatch
      setopt EXTENDED_HISTORY
      setopt INC_APPEND_HISTORY
      setopt HIST_FIND_NO_DUPS
      unsetopt beep
      bindkey -v
      bindkey -r '^W'
      REPORTTIME=20
      setopt +o nomatch
      setopt PROMPT_SUBST
      export KEYTIMEOUT=1
      bat_read_null() { bat --paging=never };

      PS1='%F{33}%n%{$reset_color%}%F{244}@%{$reset_color%}%F{4}%m|%f%{$fg[green]%}%~%{$reset_color%}%{$fg[white]%}''${vim_mode}%'


      vim_ins_mode="%{$fg[green]%}|%{$reset_color%}"
      vim_cmd_mode="%{$fg[red]%}|%{$reset_color%}"
      vim_mode=$vim_ins_mode

      function zle-keymap-select {
        vim_mode="''${''${KEYMAP/vicmd/''${vim_cmd_mode}}/(main|viins)/''${vim_ins_mode}}"
      zle reset-prompt
      }

      function zle-line-finish {
        vim_mode=$vim_ins_mode
      }

      zle -N zle-line-finish
      zle -N zle-keymap-select

      if [ -z "$TMUX" ]; then
        figlet_output=$(figlet -f eftirobot 'H. & G.')
        adjusted_output=$(echo "$figlet_output" | sed '5s/\(.\{16\}\)/\1 /')
        echo -e "\e[1;35m$adjusted_output\e[0m"
      fi

      eval "$(direnv hook zsh)"

      function hashish() {
        local length=$1
        openssl rand -base64 $(( length * 3 / 4 + 1 )) | tr -dc 'a-zA-Z0-9' | head -c $length
        echo
      }

      function make() {
        git_root=$(git rev-parse --show-toplevel 2>/dev/null)

        if [ -d "$git_root" ]; then
            (cd "$git_root" && command make "$@")
        else
            command make "$@"
        fi
      }
    '';
    dirHashes = {
      dl = "$HOME/Downloads";
      nixconf = "$HOME/.config/nixos";
      bins = "$HOME/bins";
    };
  };
}
